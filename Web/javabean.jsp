<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<html>
<head>
<title>get 和 set 属性实例</title>
</head>
<body>

	<jsp:useBean id="students" class="com.yonyou.group.entity.Student">
		<jsp:setProperty name="students" property="name" value="小强" />
		<jsp:setProperty name="students" property="age" value="10" />
	</jsp:useBean>

	<p>
		学生名字:
		<jsp:getProperty name="students" property="name" />
	</p>
	</p>
	<p>
		学生年龄:
		<jsp:getProperty name="students" property="age" />
	</p>

</body>
</html>