<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>文件上传结果</title>
</head>
<body>
	<center>
		<h2>${message}</h2>
	</center>

	<%-- <jsp:include page="upload.jsp" flush="true"/> --%>

	<jsp:forward page="property.jsp" />

	<jsp:plugin type="applet" codebase="/src/com/yonyou/group/entity"
		code="MyApplet.class" width="60" height="80">
		<jsp:param name="name" value="red" />

		<jsp:fallback>
            Unable to initialize Java Plugin
        </jsp:fallback>

	</jsp:plugin>
</body>
</html>