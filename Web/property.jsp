<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<p>

	<h2>Jsp 使用 JavaBean 实例</h2>
	<jsp:useBean id="student" class="com.yonyou.group.entity.Student" />

	<jsp:setProperty name="student" property="name" value="十三香" />
	<jsp:setProperty name="student" property="age" value="23" />

	<p>输出信息....</p>

	<jsp:getProperty name="student" property="name" />
	<jsp:getProperty name="student" property="age" />

</p>