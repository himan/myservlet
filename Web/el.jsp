<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	String title = "User Agent Example";
%>
<html>
<head>
<title>
	<%
		out.print(title);
	%>
</title>
</head>
<body>
	<center>
		<h1>
			<%
				out.print(title);
			%>
		</h1>
	</center>
	<div align="center">
		<p>${param}</p>
	</div>
</body>
</html>