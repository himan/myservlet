package com.yonyou.group;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @Description: TODO
 * @author liulei
 * @date 2017年7月9日 上午10:23:27
 */
public class MyServlet extends HttpServlet {

	private String message;

	public void init() throws ServletException {
		// 执行必需的初始化
		message = "Hello World";
		System.out.println("init");
	}

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// 设置响应内容类型
		response.setContentType("text/html");
		response.setCharacterEncoding("utf-8");
		String title = "get 方法!";
		// 实际的逻辑是在这里
		PrintWriter out = response.getWriter();
		String name = new String(request.getParameter("name").getBytes("ISO8859-1"), "UTF-8");

		String docType = "<!DOCTYPE html> \n";
		out.println(
				docType + "<html>\n" + "<head><title>" + title + "</title></head>\n" + "<body bgcolor=\"#f0f0f0\">\n"
						+ "<h1 align=\"center\">" + title + "</h1>\n" + "<ul>\n" + "  <li><b>站点名</b>：" + name + "\n"
						+ "  <li><b>网址</b>：" + request.getParameter("url") + "\n" + "</ul>\n" + "</body></html>");
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

	public void destroy() {
		System.out.println("destroy");
	}
}
