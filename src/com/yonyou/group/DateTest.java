package com.yonyou.group;

import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * @Description: TODO
 * @author liulei
 * @date 2017年7月13日 下午3:42:10
 */
public class DateTest {

	public static void main(String[] args) throws ParseException {
		String date = "2017-12-1";
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		System.out.println(format.parse(date));
	}
}
