package com.yonyou.group.entity;

/**
 * @Description: TODO
 * @author liulei
 * @date 2017年7月17日 上午10:28:02
 */
public class Student {

	private String name;

	private Integer age;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

}
